//#include <SPI.h>
#include <WiFi101.h>
#include "Adafruit_MQTT.h"
#include "Adafruit_MQTT_Client.h"
#include <Adafruit_NeoPixel_ZeroDMA.h>
#include "DHT.h"

#define DHTPIN 6     // what digital pin we're connected to
#define PIN 11
#define DHTTYPE DHT21   // DHT 21 (AM2301)

// Connect pin 1 (on the left) of the sensor to +5V
// NOTE: If using a board with 3.3V logic like an Arduino Due connect pin 1
// to 3.3V instead of 5V!
// Connect pin 2 of the sensor to whatever your DHTPIN is
// Connect pin 4 (on the right) of the sensor to GROUND
// Connect a 10K resistor from pin 2 (data) to pin 1 (power) of the sensor

// Initialize DHT sensor.
// Note that older versions of this library took an optional third parameter to
// tweak the timings for faster processors.  This parameter is no longer needed
// as the current DHT reading algorithm adjusts itself to work on faster procs.
DHT dht(DHTPIN, DHTTYPE);

Adafruit_NeoPixel_ZeroDMA strip(60, PIN, NEO_GRB); //initialize led strip

//please enter your sensitive data in the Secret tab/arduino_secrets.h
char ssid[] = "IoTT-CHI-WS";        // your network SSID (name)
char pass[] = "iott2018";    // your network password (use for WPA, or use as key for WEP)

int status = WL_IDLE_STATUS;

// Initialize the Ethernet client library
// with the IP address and port of the server
// that you want to connect to (port 80 is default for HTTP):
WiFiClient client;

#define AIO_SERVER      "mqtt.humantech.institute"
#define AIO_SERVERPORT  8080                  // use 8883 for SSL
Adafruit_MQTT_Client mqtt(&client, AIO_SERVER, AIO_SERVERPORT, NULL, NULL); //NULL for username and password

/****************************** Feeds ***************************************/

// Setup a feed called 'start' for subscribing to changes.
Adafruit_MQTT_Subscribe start = Adafruit_MQTT_Subscribe(&mqtt, "start");

// Setup a feed called 'diffuseur d'odeur' for subscribing to changes.
Adafruit_MQTT_Subscribe diffuseur = Adafruit_MQTT_Subscribe(&mqtt, "Odor Diffuser");

// Setup a feed called 'leds' for subscribing to changes.
Adafruit_MQTT_Subscribe leds = Adafruit_MQTT_Subscribe(&mqtt, "Neopixel");

// Setup a feed called 'lm35' for publishing changes.
Adafruit_MQTT_Publish temperatureSensor = Adafruit_MQTT_Publish(&mqtt, "lm35");

/*************************** Sketch Code ************************************/

// Bug workaround for Arduino 1.6.6, it seems to need a function declaration
// for some reason (only affects ESP8266, likely an arduino-builder bug).
void MQTT_connect();

//Odor Diffuser
uint16_t delai = 0;
unsigned long previousMillis = 0;        // will store last time LED was updated
boolean check=false;

//Sensor
boolean beginSensor=false;
unsigned long sensorMillis=0;
uint16_t delaiSensor = 0;


void setup() {
  //Initialize serial and wait for port to open:
  Serial.begin(9600);
  Serial.println("DHTxx test!");
//  pinMode(LED_BUILTIN,OUTPUT);
//  analogWrite(LED_BUILTIN,0);
  
  strip.begin();
  strip.show(); // Initialize all pixels to 'off'
  
 //Configure pins for Adafruit ATWINC1500 Feather
  WiFi.setPins(8, 7, 4, 2);

  //Connect to WPA/WPA2 network. Change this line if using open or WEP network:
  status = WiFi.begin(ssid, pass);
  dht.begin();
   
  // Setup MQTT subscription for feed.
   mqtt.subscribe(&leds);
  mqtt.subscribe(&diffuseur);
  mqtt.subscribe(&start);
  
  
  pinMode(A5,OUTPUT);
}
void loop() {
  // Ensure the connection to the MQTT server is alive (this will make the first
  // connection and automatically reconnect when disconnected).  See the MQTT_connect
  // function definition further below.
  MQTT_connect();
  // Read temperature as Celsius (the default)
  
  if(check){
    checkInterval();
  }
  if(beginSensor){
    checkDuration();
  }
  
  // this is our 'wait for incoming subscription packets' busy subloop
  // try to spend your time here

  Adafruit_MQTT_Subscribe *subscription;
  while ((subscription = mqtt.readSubscription(5000))) {
    if (subscription == &start) {
      delaiSensor = atoi((char *)start.lastread)*1000;  // convert to a number
      float temperature = dht.readTemperature();
      temperatureSensor.publish((uint32_t)temperature);
      beginSensor=true;
      sensorMillis=millis();
      Serial.print(" %\t");
      Serial.print("Temperature: ");
      Serial.print(temperature);
    }
    else if (subscription == &diffuseur && check==false) {
      Serial.print(F("Got: "));
      Serial.println((char *)diffuseur.lastread);
      delai = atoi((char *)diffuseur.lastread)*1000;  // convert to a number
      // save the last time you blinked the LED
      previousMillis = millis();
      digitalWrite(A5,HIGH);
      check=true;
//      delay(delai);
//      digitalWrite(A5,LOW);
    }
    else if (subscription == &leds) {
      Serial.print(F("Got: "));
      Serial.println((char *)leds.lastread);
      
      if(strcmp((char *)leds.lastread,"blue")==0){
        for(int i=0;i<strip.numPixels();++i){
          strip.setPixelColor(i,0,0,255);
//        Serial.println(i);
          strip.show();
//        Serial.println(strip.getPixelColor(i));
//          delay(50);
         }
      } else if(strcmp((char *)leds.lastread,"green")==0){
        for(int i=0;i<strip.numPixels();++i){
          strip.setPixelColor(i,0,255,0);
//        Serial.println(i);
          strip.show();
//        Serial.println(strip.getPixelColor(i));
//          delay(50);
         }
      } else if(strcmp((char *)leds.lastread,"red")==0){
         for(int i=0;i<strip.numPixels();++i){
//          Serial.println("in");
          strip.setPixelColor(i,255,0,0);
//        Serial.println(i);
          strip.show();
//        Serial.println(strip.getPixelColor(i));
//          delay(50);
         }
      }
     
    }
  }
}
void checkInterval(){
  unsigned long currentMillis = millis();
  if (currentMillis - previousMillis >= delai) {
    digitalWrite(A5,LOW);
    check=false;
    delai=0;
    for(int i=0;i<strip.numPixels();++i){
      strip.setPixelColor(i,0,0,0);
//      Serial.println(i);
      strip.show();
//      Serial.println(strip.getPixelColor(i));
    }
  }
    
}

void checkDuration(){
  unsigned long currentMillis = millis();
  if(currentMillis-sensorMillis < delaiSensor){
    float temperature = dht.readTemperature();
    temperatureSensor.publish((uint32_t)temperature);
    Serial.print(" %\t");
    Serial.print("Temperature: ");
    Serial.print(temperature);
  }
}
  
 // Function to connect and reconnect as necessary to the MQTT server.
// Should be called in the loop function and it will take care if connecting.
void MQTT_connect() {
  int8_t ret;

  // Stop if already connected.
  if (mqtt.connected()) {
    return;
  }

  Serial.print("Connecting to MQTT... ");

  uint8_t retries = 3;
  while ((ret = mqtt.connect()) != 0) { // connect will return 0 for connected
       Serial.println(mqtt.connectErrorString(ret));
       Serial.println("Retrying MQTT connection in 5 seconds...");
       mqtt.disconnect();
       delay(5000);  // wait 5 seconds
       retries--;
       if (retries == 0) {
         // basically die and wait for WDT to reset me
         while (1);
       }
  }
  Serial.println("MQTT Connected!");
}



