
Nom du projet
=============

Infos g�n�rales
---------------

- **Etudiant** : Sara Semaan- sara.semaan@edu.hefr.ch
- **Superviseur** : [Leonardo Angelini](https://gitlab.forge.hefr.ch/leonardo.angelini) - leonardo.angelini@hefr.ch
- **Professeur** : Omar AbouKhaled - omar.aboukhaled@hefr.ch
- **Dates** : du 02.07.2018 au 04.09.2018


Contexte
--------

L�exp�rience multi sensorielle culinaire est d�velopp�e dans le cadre d�un stage en 4�me ann�e d�ing�nieur informatique et t�l�communications-option g�nie logiciel et se d�roule � l�Haute �cole d'ing�nierie et d'architecture de Fribourg.


Description
-----------

L�objectif de ce projet est de combiner la technologie � l�exp�rience culinaire pour permettre de montrer que le go�t d�un plat d�pend de la situation dans laquelle se trouve la personne. 


Contenu
-------

Ce d�p�t contient toute la documentation relative au projet dans le dossier `docs/`. Le code du projet est dans le dossier `code/`.