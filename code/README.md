Nom du projet
=============

Phrase qui d�crit le but de ce projet.


Pr�requis
---------

- Python 3.5
- Installer MongoDB en local
- NodeJS


Installation
------------

Pour installer les d�pendances :

    npm install


Lancement
---------

Pour lancer l'API 

    python main.py

Pour lancer le frontend

    node main.js
