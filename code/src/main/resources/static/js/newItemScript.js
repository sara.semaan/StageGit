function load() {
    document.getElementById('modalForm').style.display='block';
    document.getElementById("ledNumbers").style.display = "none";
    document.getElementById("typeActuator").style.display = "none";
    document.getElementById("url").style.display = "none";
}

function showFields() {

    var x = document.getElementById("typeSelection").value;
    switch (x) {
        case "led":
            document.getElementById("ledNumbers").style.display = "block";
            document.getElementById("typeSensor").style.display = "none";
            document.getElementById("sensorThreshold").style.display = "none";
            document.getElementById("typeActuator").style.display = "none";
            document.getElementById("url").style.display = "none";
            break;
        case "actuator":
            document.getElementById("ledNumbers").style.display = "none";
            document.getElementById("typeSensor").style.display = "none";
            document.getElementById("sensorThreshold").style.display = "none";
            document.getElementById("typeActuator").style.display = "block";
            document.getElementById("url").style.display = "none";

            break;
        case "sensor":
            document.getElementById("ledNumbers").style.display = "none";
            document.getElementById("typeSensor").style.display = "block";
            document.getElementById("sensorThreshold").style.display = "block";
            document.getElementById("typeActuator").style.display = "none";
            document.getElementById("url").style.display = "none";
            break;
        case "video":
            document.getElementById("ledNumbers").style.display = "none";
            document.getElementById("typeSensor").style.display = "none";
            document.getElementById("sensorThreshold").style.display = "none";
            document.getElementById("typeActuator").style.display = "none";
            document.getElementById("url").style.display = "block";
       }
}
