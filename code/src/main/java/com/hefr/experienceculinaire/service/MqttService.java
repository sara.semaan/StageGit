package com.hefr.experienceculinaire.service;

import com.hefr.experienceculinaire.entities.MqttSensorMessage;
import com.hefr.experienceculinaire.repositories.MqttSensorMessageRepository;
import org.eclipse.paho.client.mqttv3.*;
import org.eclipse.paho.client.mqttv3.persist.MqttDefaultFilePersistence;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

@Service
public class MqttService implements MqttCallback {

    private MqttClient client;
    private String brokerUrl;
    private String clientId;
    private MqttConnectOptions conOpt;
    private final String broker = "mqtt.humantech.institute";
    private final int port = 8080;
    private final String protocol = "tcp://";

    private MqttSensorMessageRepository mqttSensorMessageRepository;

    public MqttClient getClient() {
        return client;
    }


    @Autowired
    public MqttService(MqttSensorMessageRepository mqttSensorMessageRepository) {
        this();
        this.mqttSensorMessageRepository = mqttSensorMessageRepository;
    }

    public MqttService() {
        this.brokerUrl = protocol + broker + ":" + port;
        this.clientId = "Id#" + (new Random()).nextInt(200);
        String tmpDir = System.getProperty("java.io.tmpdir");
        MqttDefaultFilePersistence dataStore;
        dataStore = new MqttDefaultFilePersistence(tmpDir);

//        messages = new ArrayList<>();

        try {
            conOpt = new MqttConnectOptions();
            conOpt.setCleanSession(true);
            client = new MqttClient(this.brokerUrl, clientId, dataStore);
            client.setCallback(this);

        } catch (MqttException e) {
            e.printStackTrace();
            log("Unable to set up client: " + e.toString());
            System.exit(1);

        }
    }

    @Override
    public void connectionLost(Throwable cause) {
        log("Connection to " + brokerUrl + " lost!" + cause);
        System.exit(1);
    }

    public void saveOrUpdate(MqttSensorMessage mqttSensorMessage) {
        mqttSensorMessageRepository.save(mqttSensorMessage);
    }

    public void deleteById(long id) {
        mqttSensorMessageRepository.deleteById(id);
    }


    @Override
    public void messageArrived(String topic, MqttMessage message) {
//        if(Double.parseDouble(message.toString())>WrapperValue.getValue())
        MqttSensorMessage mqttSensorMessage = new MqttSensorMessage(topic, message.toString());
        mqttSensorMessageRepository.save(mqttSensorMessage);
    }



    public String getBrokerUrl() {
        return brokerUrl;
    }

    public MqttConnectOptions getConOpt() {
        return conOpt;
    }

    @Override
    public void deliveryComplete(IMqttDeliveryToken token) {

    }


    public List<MqttSensorMessage> listAllItems() {
        List<MqttSensorMessage> messages = new ArrayList<>();
        mqttSensorMessageRepository.findAll().forEach(messages::add);
        return Collections.unmodifiableList(messages);
    }



    public void log(String message) {
        System.out.println(message);
    }


}
