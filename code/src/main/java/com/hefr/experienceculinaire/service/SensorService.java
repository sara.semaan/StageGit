package com.hefr.experienceculinaire.service;

import com.hefr.experienceculinaire.entities.Component;
import com.hefr.experienceculinaire.entities.Sensor;
import com.hefr.experienceculinaire.repositories.SensorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Service
public class SensorService {
    private SensorRepository sensorRepository;

    @Autowired
    public SensorService(SensorRepository sensorRepository) {
        this.sensorRepository = sensorRepository;
    }

    public void saveOrUpdate(Component sensor) {

       if(sensor instanceof Sensor) sensorRepository.save((Sensor)sensor);
    }


    public void deleteById(long id) {
        sensorRepository.deleteById(id);
    }

    public Sensor findById(long id) {
        return sensorRepository.findById(id).orElse(null);
    }

    public List<Sensor> listAllItems() {
        List<Sensor> sensors = new ArrayList<>();

        sensorRepository.findAll().forEach(sensors::add);

        return Collections.unmodifiableList(sensors);
    }
}
