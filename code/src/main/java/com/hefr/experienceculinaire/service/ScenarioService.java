package com.hefr.experienceculinaire.service;

import com.hefr.experienceculinaire.entities.Actuator;
import com.hefr.experienceculinaire.entities.Component;
import com.hefr.experienceculinaire.entities.Scenario;
import com.hefr.experienceculinaire.repositories.ActuatorRepository;
import com.hefr.experienceculinaire.repositories.ScenarioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Service
public class ScenarioService {
    private ScenarioRepository scenarioRepository;

    @Autowired
    public ScenarioService(ScenarioRepository scenarioRepository) {
        this.scenarioRepository = scenarioRepository;
    }

    public void saveOrUpdate(Scenario scenario) {
        scenarioRepository.save(scenario);
    }

    public Scenario findById(long id) {
        return scenarioRepository.findById(id).orElse(null);
    }

    public void deleteById(long id) {
        scenarioRepository.deleteById(id);
    }


    public List<Scenario> listAllItems() {
        List<Scenario> scenarios = new ArrayList<>();

        scenarioRepository.findAll().forEach(scenarios::add);

        return Collections.unmodifiableList(scenarios);
    }
}
