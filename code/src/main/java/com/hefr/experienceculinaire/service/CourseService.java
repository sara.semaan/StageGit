package com.hefr.experienceculinaire.service;

import com.hefr.experienceculinaire.entities.Actuator;
import com.hefr.experienceculinaire.entities.Component;
import com.hefr.experienceculinaire.entities.Course;
import com.hefr.experienceculinaire.repositories.CourseRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Service
public class CourseService {
    private CourseRepository courseRepository;

    @Autowired

    public CourseService(CourseRepository courseRepository) {
        this.courseRepository = courseRepository;
    }

    public void saveOrUpdate(Course course) {
        courseRepository.save(course);
    }


    public void deleteById(long id) {
        courseRepository.deleteById(id);
    }

    public Course findById(long id) {
        return courseRepository.findById(id).orElse(null);
    }


    public List<Course> listAllItems() {
        List<Course> courses = new ArrayList<>();

        courseRepository.findAll().forEach(courses::add);

        return Collections.unmodifiableList(courses);
    }

}
