package com.hefr.experienceculinaire.service;

import com.hefr.experienceculinaire.entities.Component;
import com.hefr.experienceculinaire.entities.Led;
import com.hefr.experienceculinaire.repositories.LedRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Service
public class LedService {
    private LedRepository ledRepository;

    @Autowired
    public LedService(LedRepository ledRepository) {
        this.ledRepository = ledRepository;
    }

    public void saveOrUpdate(Component led) {
        if (led instanceof Led) ledRepository.save((Led) led);
    }


    public void deleteById(long id) {
        ledRepository.deleteById(id);
    }

    public Led findById(long id) {
        return ledRepository.findById(id).orElse(null);
    }


    public List<Led> listAllItems() {
        List<Led> leds = new ArrayList<>();

        ledRepository.findAll().forEach(leds::add);

        return Collections.unmodifiableList(leds);
    }
}
