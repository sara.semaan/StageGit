package com.hefr.experienceculinaire.service;

import com.hefr.experienceculinaire.entities.Component;
import com.hefr.experienceculinaire.entities.Video;
import com.hefr.experienceculinaire.repositories.VideoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Service
public class VideoService {
    private VideoRepository videoRepository;

    @Autowired
    public VideoService(VideoRepository videoRepository) {
        this.videoRepository = videoRepository;
    }
    public void saveOrUpdate(Component video) {
        if (video instanceof Video) videoRepository.save((Video) video);
    }


    public void deleteById(long id) {
        videoRepository.deleteById(id);
    }

    public Video findById(long id) {
        return videoRepository.findById(id).orElse(null);
    }


    public List<Video> listAllItems() {
        List<Video> videos = new ArrayList<>();

        videoRepository.findAll().forEach(videos::add);

        return Collections.unmodifiableList(videos);
    }
}
