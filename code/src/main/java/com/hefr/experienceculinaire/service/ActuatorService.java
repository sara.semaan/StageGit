package com.hefr.experienceculinaire.service;

import com.hefr.experienceculinaire.entities.Actuator;
import com.hefr.experienceculinaire.entities.Component;
import com.hefr.experienceculinaire.repositories.ActuatorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Service
public class ActuatorService {
    private ActuatorRepository actuatorRepository;

    @Autowired
    public ActuatorService(ActuatorRepository actuatorRepository) {
        this.actuatorRepository = actuatorRepository;
    }


    public void saveOrUpdate(Component actuator) {
        if (actuator instanceof Actuator) actuatorRepository.save((Actuator) actuator);
    }


    public void deleteById(long id) {
        actuatorRepository.deleteById(id);
    }

    public Actuator findById(long id) {
        return actuatorRepository.findById(id).orElse(null);
    }


    public List<Actuator> listAllItems() {
        List<Actuator> actuators = new ArrayList<>();

        actuatorRepository.findAll().forEach(actuators::add);

        return Collections.unmodifiableList(actuators);
    }

}
