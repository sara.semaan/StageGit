package com.hefr.experienceculinaire.controller;

import com.hefr.experienceculinaire.data.*;
import com.hefr.experienceculinaire.entities.*;
import com.hefr.experienceculinaire.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;


@Controller
public class CulinaryExperiencesController {

    @Autowired
    private ActuatorService actuatorService;
    @Autowired
    private SensorService sensorService;
    @Autowired
    private LedService ledService;
    @Autowired
    private ScenarioService scenarioService;
    @Autowired
    private CourseService courseService;
    @Autowired
    private VideoService videoService;
    @Autowired
    private MqttService mqttService;
    private Subscriber subscriber;
    private BigCourse bigCourse;
    private long id;
    private int compteur = 2;
    private Course course;

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String index(Model m) {
        subscriber = new Subscriber(mqttService);
        BigCourse.setSubscriber(subscriber);
        m.addAttribute("scenarios", scenarioService.listAllItems());
        return "index";
    }

    @RequestMapping(value = "/about", method = RequestMethod.GET)
    public String about() {
        return "about";
    }

    @RequestMapping(value = "/messages", method = RequestMethod.GET)
    public String messages(Model m) {
        m.addAttribute("messages",mqttService.listAllItems());
        return "displayMessages";
    }


    @RequestMapping(value = "/createComponent", method = RequestMethod.GET)
    public String createComponent(Model m) {
//        actuatorService.saveOrUpdate(new Actuator("diffuseur d'odeur", "bla bla bla", "diffuseur"));
//        sensorService.saveOrUpdate(new Sensor("123", "123", "Temperature Sensor", 20));
//        ledService.saveOrUpdate(new Led("led1", "rgb leds", 50));
        UserEntry userEntry = new UserEntry();
        m.addAttribute("userEntry", userEntry);
        m.addAttribute("actuators", actuatorService.listAllItems());
        m.addAttribute("sensors", sensorService.listAllItems());
        m.addAttribute("leds", ledService.listAllItems());
        m.addAttribute("videos",videoService.listAllItems());
        return "createComponent";
    }


    @RequestMapping(value = "/createScenario", method = RequestMethod.GET)
    public String createScenario(Model m) {
        UserEntry userEntry = new UserEntry();
//        courseService.saveOrUpdate(new Course("course1", "this is the first course", "hello1", "bye1"));
//        courseService.saveOrUpdate(new Course("course2", "this is the second course", "hello2", "bye2"));
        m.addAttribute("userEntry", userEntry);
        m.addAttribute("courses", courseService.listAllItems());
        return "createScenario";
    }

    @RequestMapping(value = "/submitScenario", method = RequestMethod.POST)
    public String submitScenario(@ModelAttribute(value = "userEntry") UserEntry userEntry) {
        ScenarioFactory scenarioFactory = new ScenarioFactory(userEntry);
        Scenario scenario = scenarioFactory.createScenario();
        scenario.setCoursesList(courseService.listAllItems());
        scenarioService.saveOrUpdate(scenario);
        return "redirect:/";
    }

    @RequestMapping(value = "/createItem", method = RequestMethod.POST)
    public String createItem(@ModelAttribute(value = "userEntry") UserEntry userEntry) {
        ComponentFactory componentFactory = new ComponentFactory(userEntry);
        Component item = componentFactory.createComponent();
        if (item instanceof Actuator) {
            actuatorService.saveOrUpdate(item);
        } else if (item instanceof Sensor) {
            sensorService.saveOrUpdate(item);
        } else if (item instanceof Led) {
            ledService.saveOrUpdate(item);
        } else if (item instanceof Video){
            videoService.saveOrUpdate(item);
        }
        return "redirect:/createComponent";
    }

    @RequestMapping(value = "/createCourse", method = RequestMethod.POST)
    public String createCourse(@RequestParam(value = "sensorName", required = false) String[] sensorValue, @RequestParam(value = "actuatorName", required = false) String[] actuatorValue, @RequestParam(value = "ledName", required = false) String[] ledValue, @RequestParam(value = "videoName", required = false) String videoValue, @ModelAttribute(value = "userEntry") UserEntry userEntry) {
        if (sensorValue != null) {
            for (int i = 0; i < sensorValue.length; ++i) {
                userEntry.getCourseEntry().addSensor(sensorService.findById(Long.parseLong(sensorValue[i])));
            }
        }
        if (actuatorValue != null) {
            for (int i = 0; i < actuatorValue.length; ++i) {
                userEntry.getCourseEntry().addActuator(actuatorService.findById(Long.parseLong(actuatorValue[i])));
            }
        }
        if (ledValue != null) {
            for (int i = 0; i < ledValue.length; ++i) {
                userEntry.getCourseEntry().addLed(ledService.findById(Long.parseLong(ledValue[i])));
            }
        }
        if(videoValue!=null){
            userEntry.getCourseEntry().setVideo(videoService.findById(Long.parseLong(videoValue)));
        }
        CourseFactory courseFactory = new CourseFactory(userEntry);
        course = courseFactory.createCourse();
        courseService.saveOrUpdate(course);
        return "redirect:/createScenario";
    }


    @RequestMapping(value = "/deleteActuator", method = RequestMethod.GET)
    public String deleteActuator(@RequestParam(value = "id") String id) {
        actuatorService.deleteById(Long.parseLong(id));
        return "redirect:/createComponent";
    }

    @RequestMapping(value = "/deleteLed", method = RequestMethod.GET)
    public String deleteGameObject(@RequestParam(value = "id") String id) {
        ledService.deleteById(Long.parseLong(id));
        return "redirect:/createComponent";
    }

    @RequestMapping(value = "/deleteSensor", method = RequestMethod.GET)
    public String deleteSensor(@RequestParam(value = "id") String id) {
        sensorService.deleteById(Long.parseLong(id));
        return "redirect:/createComponent";
    }
    @RequestMapping(value = "/deleteVideo", method = RequestMethod.GET)
    public String deleteVideo(@RequestParam(value = "id") String id) {
        videoService.deleteById(Long.parseLong(id));
        return "redirect:/createComponent";
    }

    @RequestMapping(value = "/deleteMessage", method = RequestMethod.GET)
    public String deleteMessage(@RequestParam(value = "id") String id) {
        mqttService.deleteById(Long.parseLong(id));
        return "redirect:/messages";
    }

    @RequestMapping(value = "/synchronizeExperience", method = RequestMethod.GET)
    public String synchronizeExperience(@RequestParam(value = "id") String id, Model m) {
        m.addAttribute("courses", scenarioService.findById(Long.parseLong(id)).getCoursesList());
        return "synchronizeExperience";
    }

    @RequestMapping(value = "/synchronizeCourse", method = RequestMethod.GET)
    public String synchronizeCourse(@RequestParam(value = "id") String id, Model m) {
        m.addAttribute("actuators", courseService.findById(Long.parseLong(id)).getActuatorList());
        m.addAttribute("leds", courseService.findById(Long.parseLong(id)).getLedList());
        m.addAttribute("sensors", courseService.findById(Long.parseLong(id)).getSensorList());
        m.addAttribute("course", courseService.findById(Long.parseLong(id)));
        m.addAttribute("video",courseService.findById(Long.parseLong(id)).getVideo());
        System.out.println("duration "+courseService.findById(Long.parseLong(id)).getDuration());
        m.addAttribute("bigCourseEntry", new BigCourseEntry());
        return "synchronizeCourse";
    }


    //@RequestParam(value = "color") String color, @RequestParam(value = "range-min") String[] rangeMin, @RequestParam(value = "range-max") String[] rangeMax, @RequestParam(value = "id") String id, Model m
    @RequestMapping(value = "/beginCourse", method = RequestMethod.GET)
    public String beginCourse(@ModelAttribute(value = "bigCourseEntry") BigCourseEntry bigCourseEntry, @RequestParam(value = "id") String id,Model m) {

        this.id = Long.parseLong(id);
        this.bigCourse = new BigCourse(courseService.findById(this.id));
        bigCourse.addStartTime(bigCourseEntry.getStartTime());
        bigCourse.addEndTime(bigCourseEntry.getEndTime());
        bigCourse.addStartTime(bigCourseEntry.getStartTime2());
        bigCourse.addEndTime(bigCourseEntry.getEndTime2());
        bigCourse.addStartTime(bigCourseEntry.getStartTime3());
        bigCourse.addEndTime(bigCourseEntry.getEndTime3());
        bigCourse.setColor(bigCourseEntry.getColor());

        Thread thread = new Thread(bigCourse, "thread1");
        if (compteur == 1) {
            compteur = 3;
            thread.start();
        }
        compteur--;
        m.addAttribute("video",courseService.findById(this.id).getVideo().getLinkVideo());

        return "playCourse";
    }

//    @RequestMapping(value = "/publish", method = RequestMethod.POST)
//    public String publish(){
//        System.out.println("gayvin was here");
//        return "redirect:/playCourse";
//    }

//    @RequestMapping(value = "/playCourse", method = RequestMethod.GET)
//    public String playCourse() {
//
//        return "playCourse";
//    }


}
