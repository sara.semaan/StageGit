package com.hefr.experienceculinaire.data;

import com.hefr.experienceculinaire.entities.*;

import java.util.ArrayList;
import java.util.List;

public class CourseEntry {
    private String startMessage;
    private String endMessage;
    private int duration;

    private List<Sensor> sensorList;
    private List<Led> ledList;
    private List<Actuator> actuatorList;
    private Video video;

    public CourseEntry() {
        startMessage="";
        endMessage="";
        sensorList = new ArrayList<>();
        ledList = new ArrayList<>();
        actuatorList = new ArrayList<>();
        video=new Video();
    }

    public String getStartMessage() {
        return startMessage;
    }

    public void setStartMessage(String startMessage) {
        this.startMessage = startMessage;
    }

    public String getEndMessage() {
        return endMessage;
    }

    public void setEndMessage(String endMessage) {
        this.endMessage = endMessage;
    }

    public List<Sensor> getSensorList() {
        return sensorList;
    }

    public void setSensorList(List<Sensor> sensorList) {
        this.sensorList = sensorList;
    }

    public List<Led> getLedList() {
        return ledList;
    }

    public void setLedList(List<Led> ledList) {
        this.ledList = ledList;
    }

    public List<Actuator> getActuatorList() {
        return actuatorList;
    }

    public void setActuatorList(List<Actuator> actuatorList) {
        this.actuatorList = actuatorList;
    }

    public void addSensor(Sensor sensor){
        sensorList.add(sensor);
    }
    public void addLed(Led led){
        ledList.add(led);
    }
    public void addActuator(Actuator actuator){
        actuatorList.add(actuator);
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public Video getVideo() {
        return video;
    }

    public void setVideo(Video video) {
        this.video = video;
    }
}
