package com.hefr.experienceculinaire.data;


public class BigCourseEntry {
    private int startTime;
    private int endTime;
    private int startTime2;
    private int endTime2;
    private int startTime3;
    private int endTime3;
    private String color;


    public BigCourseEntry(){

    }

    public int getStartTime2() {
        return startTime2;
    }

    public void setStartTime2(int startTime2) {
        this.startTime2 = startTime2;
    }

    public int getEndTime2() {
        return endTime2;
    }

    public void setEndTime2(int endTime2) {
        this.endTime2 = endTime2;
    }

    public int getStartTime3() {
        return startTime3;
    }

    public void setStartTime3(int startTime3) {
        this.startTime3 = startTime3;
    }

    public int getEndTime3() {
        return endTime3;
    }

    public void setEndTime3(int endTime3) {
        this.endTime3 = endTime3;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public int getStartTime() {
        return startTime;
    }

    public void setStartTime(int startTime) {
        this.startTime = startTime;
    }

    public int getEndTime() {
        return endTime;
    }

    public void setEndTime(int endTime) {
        this.endTime = endTime;
    }
}
