package com.hefr.experienceculinaire.data;

import org.thymeleaf.util.StringUtils;

import java.awt.*;
import java.util.List;

public class UserEntry {
    private String componentType;
    private ActuatorEntry actuatorEntry;
    private SensorEntry sensorEntry;
    private LedEntry ledEntry;
    private String name;
    private String description;
    private ScenarioEntry scenarioEntry;
    private CourseEntry courseEntry;
    private VideoEntry videoEntry;

    public UserEntry() {
        this.actuatorEntry = new ActuatorEntry();
        this.sensorEntry = new SensorEntry();
        this.ledEntry = new LedEntry();
        componentType="";
        scenarioEntry=new ScenarioEntry();
        courseEntry=new CourseEntry();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public ActuatorEntry getActuatorEntry() {
        return actuatorEntry;
    }

    public void setActuatorEntry(ActuatorEntry actuatorEntry) {
        this.actuatorEntry = actuatorEntry;
    }

    public SensorEntry getSensorEntry() {
        return sensorEntry;
    }

    public void setSensorEntry(SensorEntry sensorEntry) {
        this.sensorEntry = sensorEntry;
    }

    public LedEntry getLedEntry() {
        return ledEntry;
    }

    public void setLedEntry(LedEntry ledEntry) {
        this.ledEntry = ledEntry;
    }

    public String getComponentType() {
        return componentType;
    }

    public void setComponentType(String componentType) {
        this.componentType = componentType;
    }

    public ScenarioEntry getScenarioEntry() {
        return scenarioEntry;
    }

    public void setScenarioEntry(ScenarioEntry scenarioEntry) {
        this.scenarioEntry = scenarioEntry;
    }

    public CourseEntry getCourseEntry() {
        return courseEntry;
    }

    public void setCourseEntry(CourseEntry courseEntry) {
        this.courseEntry = courseEntry;
    }

    public VideoEntry getVideoEntry() {
        return videoEntry;
    }

    public void setVideoEntry(VideoEntry videoEntry) {
        this.videoEntry = videoEntry;
    }
}
