package com.hefr.experienceculinaire.data;

import com.hefr.experienceculinaire.entities.Scenario;

public class ScenarioFactory {
    private UserEntry userEntry;

    public ScenarioFactory(UserEntry userEntry) {
        this.userEntry = userEntry;
    }

    public Scenario createScenario(){
        Scenario scenario=new Scenario();
        scenario.setName(userEntry.getName());
        scenario.setDescription(userEntry.getDescription());
        return scenario;
    }
}
