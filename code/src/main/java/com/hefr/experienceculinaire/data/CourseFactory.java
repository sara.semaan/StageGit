package com.hefr.experienceculinaire.data;

import com.hefr.experienceculinaire.entities.*;
import com.hefr.experienceculinaire.service.ActuatorService;
import com.hefr.experienceculinaire.service.LedService;
import com.hefr.experienceculinaire.service.SensorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

public class CourseFactory {
    private UserEntry userEntry;

    public CourseFactory(UserEntry userEntry) {
        this.userEntry = userEntry;
    }

    public Course createCourse() {
        Course course = new Course();
        course.setStartMessage(userEntry.getCourseEntry().getStartMessage());
        course.setEndMessage(userEntry.getCourseEntry().getEndMessage());
        course.setName(userEntry.getName());
        course.setDescription(userEntry.getDescription());
        course.setActuatorList(userEntry.getCourseEntry().getActuatorList());
        course.setLedList(userEntry.getCourseEntry().getLedList());
        course.setSensorList(userEntry.getCourseEntry().getSensorList());
        course.setVideo(userEntry.getCourseEntry().getVideo());
        course.setDuration(userEntry.getCourseEntry().getDuration());

        return course;
    }


}
