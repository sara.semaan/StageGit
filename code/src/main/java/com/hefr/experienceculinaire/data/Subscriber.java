package com.hefr.experienceculinaire.data;

import com.hefr.experienceculinaire.service.MqttService;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.springframework.beans.factory.annotation.Autowired;

public class Subscriber {
    private MqttService mqttService;


    @Autowired
    public Subscriber(MqttService mqttService) {
        this.mqttService = mqttService;
    }


    public void subscribe(String topicName, int qos) {
        try {
            mqttService.getClient().connect(mqttService.getConOpt());
            mqttService.log("Connected to " + mqttService.getBrokerUrl() + " with client ID " + mqttService.getClient().getClientId());
            mqttService.log("Subscribing to topic \"" + topicName + "\" with qos " + qos);
            mqttService.getClient().subscribe(topicName, qos);
        } catch (MqttException mqttException) {
            System.out.println(mqttException.getMessage() + "error subscribing");
        }


    }

    public void disconnect() {
        try {
            mqttService.getClient().disconnect();
            mqttService.log("Disconnected from subscriber");
        } catch (MqttException mqttException) {

        }
    }


    public MqttService getMqttService() {
        return mqttService;
    }


}
