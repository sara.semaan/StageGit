package com.hefr.experienceculinaire.data;

public class VideoEntry {
    private String link;

    public VideoEntry() {
        link="";
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }
}
