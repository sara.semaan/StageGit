package com.hefr.experienceculinaire.data;

import com.hefr.experienceculinaire.entities.*;

public class ComponentFactory {

    private UserEntry userEntry;

    public Component createComponent() {
        switch (userEntry.getComponentType()) {
            case "sensor":
                return new Sensor(userEntry.getName(), userEntry.getDescription(), userEntry.getSensorEntry().getType(), userEntry.getSensorEntry().getThreshold());
            case "actuator":
                return new Actuator(userEntry.getName(), userEntry.getDescription(),userEntry.getActuatorEntry().getType());

            case "led":
                return new Led(userEntry.getName(), userEntry.getDescription(), userEntry.getLedEntry().getNumberOfLeds(),"");

            case "video":
                return new Video(userEntry.getName(),userEntry.getDescription(),userEntry.getVideoEntry().getLink());
            default:
                return null;

        }
    }

    public ComponentFactory(UserEntry userEntry) {
        this.userEntry = userEntry;
    }
}
