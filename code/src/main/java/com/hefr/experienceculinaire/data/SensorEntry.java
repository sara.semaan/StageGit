package com.hefr.experienceculinaire.data;

public class SensorEntry {
    private String type;
    private double threshold;

    public SensorEntry() {
        type="";
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public double getThreshold() {
        return threshold;
    }

    public void setThreshold(double threshold) {
        this.threshold = threshold;
    }
}
