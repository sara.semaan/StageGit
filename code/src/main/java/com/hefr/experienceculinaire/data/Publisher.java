package com.hefr.experienceculinaire.data;

import com.hefr.experienceculinaire.service.MqttService;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.springframework.beans.factory.annotation.Autowired;

import java.sql.Timestamp;

public class Publisher {
    private MqttService mqttService;

    @Autowired
    public Publisher(MqttService mqttService){
        this.mqttService = mqttService;
    }
    public Publisher() {
        mqttService = new MqttService();

    }
    public MqttService getMqttService() {
        return mqttService;
    }

    public void publish(String topicName, int qos, byte[] payload) throws MqttException {
        mqttService.log("Connected to " + mqttService.getBrokerUrl() + " with client ID " + mqttService.getClient().getClientId());
        mqttService.getClient().connect(mqttService.getConOpt());
        mqttService.log("Connected");
        String time = new Timestamp(System.currentTimeMillis()).toString();
        mqttService.log("Publishing at: " + time + " to topic \"" + topicName + "\" qos " + qos);
        MqttMessage message = new MqttMessage(payload);
        message.setQos(qos);
        mqttService.getClient().publish(topicName, message);
        mqttService.getClient().disconnect();
        mqttService.log("Disconnected");
    }

//    public static void main(String[] args) throws MqttException {
//        MqttService mqttService = new MqttService();
//        Publisher publisher = new Publisher(mqttService);
//        publisher.publish(mqttService.getTopic(),1,mqttService.getMessage().getBytes());
//
//    }

}
