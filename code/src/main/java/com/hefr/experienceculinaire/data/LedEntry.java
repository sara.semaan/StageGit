package com.hefr.experienceculinaire.data;

public class LedEntry {
    private int numberOfLeds;
    private String color;

    public LedEntry() {
        this.numberOfLeds = 0;
    }

    public int getNumberOfLeds() {
        return numberOfLeds;
    }

    public void setNumberOfLeds(int numberOfLeds) {
        this.numberOfLeds = numberOfLeds;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }
}
