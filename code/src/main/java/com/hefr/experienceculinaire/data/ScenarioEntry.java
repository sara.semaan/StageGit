package com.hefr.experienceculinaire.data;

import com.hefr.experienceculinaire.entities.Course;

import java.util.ArrayList;
import java.util.List;

public class ScenarioEntry {
    private List<Course> coursesList;

    public ScenarioEntry() {
        coursesList=new ArrayList<>();
    }

    public List<Course> getCoursesList() {
        return coursesList;
    }

    public void setCoursesList(List<Course> coursesList) {
        this.coursesList = coursesList;
    }

}
