package com.hefr.experienceculinaire.data;

public class ActuatorEntry {
    private String type;

    public ActuatorEntry() {
        this.type = "";
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

}
