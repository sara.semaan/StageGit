package com.hefr.experienceculinaire.entities;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "videoTable")
public class Video extends Component{
    private String linkVideo;

    public Video() {
    }

    public Video(String linkVideo) {
        this.linkVideo = linkVideo;
    }

    public Video(String name, String description, String linkVideo) {
        super(name, description);
        this.linkVideo = linkVideo;
    }

    public String getLinkVideo() {
        return linkVideo;
    }

    public void setLinkVideo(String linkVideo) {
        this.linkVideo = linkVideo;
    }
}
