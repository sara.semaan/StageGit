package com.hefr.experienceculinaire.entities;

import com.hefr.experienceculinaire.data.Publisher;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "courseTable")
public class Course extends Component {
    private String startMessage;
    private String endMessage;
    private int duration;

    @OneToOne(cascade = CascadeType.ALL)
    private Video video;
    @LazyCollection(LazyCollectionOption.FALSE)
    @OneToMany(cascade = CascadeType.ALL)
    private List<Sensor> sensorList;

    @LazyCollection(LazyCollectionOption.FALSE)
    @OneToMany(cascade = CascadeType.ALL)
    private List<Led> ledList;

    @OneToMany(fetch = FetchType.EAGER,cascade = CascadeType.ALL)
    private List<Actuator> actuatorList;

    public Course() {
    }

    public Course(String startMessage, String endMessage, int duration) {
        this.startMessage = startMessage;
        this.endMessage = endMessage;
        this.duration = duration;
        sensorList = new ArrayList<>();
        ledList = new ArrayList<>();
        actuatorList = new ArrayList<>();
        video=new Video();
    }

    public Course(String name, String description, String startMessage, String endMessage, int duration) {
        super(name, description);
        this.startMessage = startMessage;
        this.endMessage = endMessage;
        this.duration = duration;
        sensorList = new ArrayList<>();
        ledList = new ArrayList<>();
        actuatorList = new ArrayList<>();
        video=new Video();
    }

    public void setStartMessage(String startMessage) {
        this.startMessage = startMessage;
    }

    public void setEndMessage(String endMessage) {
        this.endMessage = endMessage;
    }

    public String getStartMessage() {
        return startMessage;
    }

    public String getEndMessage() {
        return endMessage;
    }

    public List<Sensor> getSensorList() {
        return sensorList;
    }

    public void setSensorList(List<Sensor> sensorList) {
        this.sensorList = sensorList;
    }

    public List<Led> getLedList() {
        return ledList;
    }

    public void setLedList(List<Led> ledList) {
        this.ledList = ledList;
    }

    public List<Actuator> getActuatorList() {
        return actuatorList;
    }

    public void setActuatorList(List<Actuator> actuatorList) {
        this.actuatorList = actuatorList;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public Video getVideo() {
        return video;
    }

    public void setVideo(Video video) {
        this.video = video;
    }
}
