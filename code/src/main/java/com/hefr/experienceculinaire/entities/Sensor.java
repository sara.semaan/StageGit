package com.hefr.experienceculinaire.entities;

import javax.persistence.*;

@Entity
@Table(name = "sensorTable")
@Embeddable
public class Sensor extends Component {

    private String type;
    private double threshold;

    public Sensor() {
    }

    public Sensor(String name, String description, String type, double threshold) {
        super(name, description);
        this.type = type;
        this.threshold = threshold;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public double getThreshold() {
        return threshold;
    }

    public void setThreshold(double threshold) {
        this.threshold = threshold;
    }
}
