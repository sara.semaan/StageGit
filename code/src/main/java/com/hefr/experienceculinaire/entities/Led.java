package com.hefr.experienceculinaire.entities;

import javax.persistence.*;

@Entity
@Table(name = "ledTable")
@Embeddable
public class Led extends Component {
    private int numberOfLeds;
    private String color;

    public Led() {

    }

    public Led(String name, String description, int numberOfLeds,String color) {
        super(name, description);
        this.numberOfLeds = numberOfLeds;
        this.color=color;
    }

    public int getNumberOfLeds() {
        return numberOfLeds;
    }

    public void setNumberOfLeds(int numberOfLeds) {
        this.numberOfLeds = numberOfLeds;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }
}
