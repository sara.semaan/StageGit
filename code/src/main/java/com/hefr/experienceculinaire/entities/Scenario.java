package com.hefr.experienceculinaire.entities;


import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.LinkedList;
import java.util.List;

@Entity
@Table(name = "scenarioTable")
public class Scenario extends Component {

    @OneToMany(cascade = CascadeType.ALL)
    private List<Course> coursesList;

    public Scenario() {

        this.coursesList = new LinkedList<>();
    }

    public Scenario(String name, String description, List<Course> coursesList) {
        super(name, description);
        this.coursesList = coursesList;
    }

    public void setCoursesList(List<Course> coursesList) {
        this.coursesList = coursesList;
    }

    public List<Course> getCoursesList() {
        return coursesList;
    }

    public void addPosition(int position, Course element) {
        coursesList.add(position, element);
    }
}
