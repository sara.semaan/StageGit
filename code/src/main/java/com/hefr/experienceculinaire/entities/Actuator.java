package com.hefr.experienceculinaire.entities;


import javax.persistence.*;

@Entity
@Table(name = "actuatorTable")
public class Actuator extends Component {
    private String type;


    public Actuator() {
    }

    public Actuator(String name, String description, String type) {
        super(name, description);
        this.type = type;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }


    @Override
    public String toString() {
        return "Actuator{" +
                "id=" + id +
                ", name='" + getName() + '\'' +
                ", description='" + getDescription() + '\'' +
                '}';
    }
}
