package com.hefr.experienceculinaire.entities;

import javax.persistence.*;

@Entity
@Table(name = "MqttSensorMessage")
public class MqttSensorMessage {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    private String topic;
    private String message;

    public MqttSensorMessage(String topic, String message) {
        this.topic = topic;
        this.message = message;
    }

    public MqttSensorMessage() {
    }

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public long getId() {
        return id;
    }
}

