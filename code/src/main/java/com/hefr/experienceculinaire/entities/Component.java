package com.hefr.experienceculinaire.entities;


import javax.persistence.*;
import java.io.Serializable;

@Entity
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
public class Component implements Serializable{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    protected long id;
    private String name;
    private String description;

    public Component() {
    }

    public Component(String name, String description) {
        this.name = name;
        this.description = description;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }


}
