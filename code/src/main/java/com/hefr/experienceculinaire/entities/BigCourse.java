package com.hefr.experienceculinaire.entities;

import com.hefr.experienceculinaire.data.Publisher;
import com.hefr.experienceculinaire.data.Subscriber;
import org.eclipse.paho.client.mqttv3.MqttException;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class BigCourse extends Component implements Runnable {
    @OneToOne
    private Course course;
    @ElementCollection
    private List<Integer> startTime = new ArrayList<>();
    @ElementCollection
    private List<Integer> endTime = new ArrayList<>();
    private String color;
    @Transient
    private static Subscriber subscriber;

    public BigCourse(Course course, List<Integer> startTime, List<Integer> endTime) {
        this.course = course;
        this.startTime = startTime;
        this.endTime = endTime;
    }

    public BigCourse(Course course) {
        this.course = course;
    }

    public void addStartTime(int time) {
        this.startTime.add(time);
    }

    public void addEndTime(int time) {
        this.endTime.add(time);
    }

    public Course getCourse() {
        return course;
    }

    public void setCourse(Course course) {
        this.course = course;
    }

    public List<Integer> getStartTime() {
        return startTime;
    }

    public void setStartTime(List<Integer> startTime) {
        this.startTime = startTime;
    }

    public List<Integer> getEndTime() {
        return endTime;
    }

    public void setEndTime(List<Integer> endTime) {
        this.endTime = endTime;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }


    public List<Integer> interval() {
        List<Integer> result = new ArrayList<>();
        for (int i = 0; i < startTime.size(); ++i) {
            result.add(endTime.get(i) - startTime.get(i));
        }
        return result;
    }

    public void publish() throws InterruptedException, MqttException {
        Publisher publisher = new Publisher();
        int time = 0;
        System.out.println("outside");
        publisher.publish("start", 1, Integer.toString(course.getDuration()).getBytes());
        System.out.println("size list " + getStartTime().size());
        for (int i = 0; i < getStartTime().size(); ++i) {
            int start = getStartTime().get(i);
            System.out.println("start" + start);
            for (int j = time; j < start; ++j) {
                Thread.sleep(1000);
                time++;
            }
            time++;
            publisher.publish(course.getActuatorList().get(0).getName(), 1, Integer.toString(interval().get(i)).getBytes());
            System.out.println("name actuator= " + course.getActuatorList().get(0).getName());
            System.out.println("interval " + Integer.toString(interval().get(i)));
            publisher.publish(course.getLedList().get(0).getName(), 1, getColor().getBytes());
            System.out.println("name led" + course.getLedList().get(0).getName());
            System.out.println("color" + course.getLedList().get(0).getColor().getBytes());


        }
        System.out.println("time= " + time);
        System.out.println("done");


    }

    public void subscribe() {
        subscriber.subscribe(course.getSensorList().get(0).getName(), 1);

    }

    public static Subscriber getSubscriber() {
        return subscriber;
    }

    public static void setSubscriber(Subscriber subscriber) {
        BigCourse.subscriber = subscriber;
    }

    public void run() {

        try {
            this.subscribe();
            this.publish();

        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (MqttException e) {
            e.printStackTrace();
        }
    }
}
