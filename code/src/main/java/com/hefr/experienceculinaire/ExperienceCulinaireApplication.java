package com.hefr.experienceculinaire;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ExperienceCulinaireApplication {

    public static void main(String[] args) {
        SpringApplication.run(ExperienceCulinaireApplication.class, args);
    }
}
