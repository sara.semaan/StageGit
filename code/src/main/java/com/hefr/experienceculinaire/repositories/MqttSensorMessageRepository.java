package com.hefr.experienceculinaire.repositories;

import com.hefr.experienceculinaire.entities.MqttSensorMessage;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MqttSensorMessageRepository extends CrudRepository<MqttSensorMessage,Long> {

}
