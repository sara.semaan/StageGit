package com.hefr.experienceculinaire.repositories;

import com.hefr.experienceculinaire.entities.Sensor;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SensorRepository extends CrudRepository<Sensor, Long> {
}
