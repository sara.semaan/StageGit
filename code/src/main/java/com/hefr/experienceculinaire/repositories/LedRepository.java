package com.hefr.experienceculinaire.repositories;

import com.hefr.experienceculinaire.entities.Led;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LedRepository extends CrudRepository<Led,Long> {
}
