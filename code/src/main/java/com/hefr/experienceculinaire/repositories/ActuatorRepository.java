package com.hefr.experienceculinaire.repositories;

import com.hefr.experienceculinaire.entities.Actuator;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface ActuatorRepository extends CrudRepository<Actuator,Long> {
}
