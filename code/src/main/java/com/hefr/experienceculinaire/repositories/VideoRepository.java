package com.hefr.experienceculinaire.repositories;

import com.hefr.experienceculinaire.entities.Video;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface VideoRepository extends CrudRepository<Video,Long> {
}
