package com.hefr.experienceculinaire.repositories;

import com.hefr.experienceculinaire.entities.Course;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CourseRepository extends CrudRepository<Course,Long> {
}
