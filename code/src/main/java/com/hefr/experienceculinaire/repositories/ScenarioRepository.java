package com.hefr.experienceculinaire.repositories;

import com.hefr.experienceculinaire.entities.Scenario;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ScenarioRepository extends CrudRepository<Scenario,Long> {
}
